<?php


	class Conexion 
    {
        function __construct( )
        {
            $this->mansion_solis = array(
                "host"      => "mysql:host=127.0.0.1;dbname=test_avicom;charset=utf8"
                ,"username" => "root"
                ,"password" => ""
            );

        }

        //Conexión a Esquema de Base de Datos ( Contactos )
        function conexion_avicom()
        {
            try
            {
                static $instance;

                if ($instance === null)
                {
                    $opt = array(
                                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                                    PDO::ATTR_EMULATE_PREPARES => FALSE
                                );

                    $instance = new PDO($this->mansion_solis['host'], $this->mansion_solis['username'], $this->mansion_solis['password'], $opt);

                }

                return $instance;
            }
            catch (PDOException $e)
            {
                echo $e->getMessage();
                return false;
            }
        }
        //Termina Conexión a Esquema de Base de Datos ( Contactos )
    }