<?php

    include ( '../class/class_headers.php' );
    include ( 'Servicios/class/sesion.php' );

    //header('Content-type: application/json; charset=utf-8');
    class_headers::obtener_headers();

    $headers  = getallheaders();
    $response = [];

    if( !isset( $headers["servicio"] ) )
    {
        $header = class_headers::validar_authorizacion();

        if( !$header["error"] )
        {
            $usuario  = base64_decode( $header["response"]->usuario );
            $password = base64_decode( $header["response"]->password );
            $response =  sesion::validar_usuario( $usuario, $password);

            header("HTTP/1.1 200 OK");
            echo json_encode( array( "session"=>$response) );

        }
        else
        {
            echo json_encode( array("accesos"=>array('error'=>false, "validaction"=>"validation", "accesos" =>$header["response"] ) ) );
        }
    }
    else
    {
        $servicio = !empty( $headers["servicio"] ) ? $headers["servicio"] : "";

        if( $servicio === 'recuperacion'  )
        {
            $data     = json_decode( file_get_contents('php://input') );
            $response =  sesion::recuperar_usuario( $data );

            header("HTTP/1.1 200 OK");
            echo json_encode( array( "session"=>$response) );
        }
        else
        {
            header("HTTP/1.1 200 OK");
            return array( "error" => true, "response"=>"Accesos de petición no permitido");
        }

    }



