<?php

    require_once( dirname(__FILE__,5).'/db/Conexion.php' );

    class modelo_session
    {
        protected $conexion;

        public function __construct()
        {
            $conexion = new Conexion();
            $this->conexion = $conexion->conexion_avicom();
        }

        public function obtener_acceso( $usuario, $password, $email, $tipo )
        {
            try
            {
                if( $tipo === 1 )
                {
                    $sql = "   SELECT   COUNT( s.usuario ) AS total
                                        ,( ( s.fecha_vigencia_final - CURRENT_DATE ) + 1 ) AS vigencia
                                        ,e.codigo_estatus
                                 FROM   usuarios s  
                            LEFT JOIN   cat_estatus e ON s.cve_estatus = e.cve_estatus
                                WHERE   s.usuario = ? 
                                  AND   s.password = MD5( ? )";

                    $vquery = $this ->
                    conexion-> prepare( $sql );
                    $vquery-> bindParam( 1, $usuario, PDO::PARAM_STR );
                    $vquery-> bindParam( 2, $password, PDO::PARAM_STR );
                    $vquery-> execute();

                    while ( $row = $vquery -> fetch(PDO::FETCH_ASSOC ) )
                    {
                        return $row;
                    }
                }
                else
                {
                    $sql = "   SELECT   s.usuario
                                        ,s.email
                                 FROM   usuarios s  
                                WHERE   s.email = LOWER ( TRIM( ? ) )";

                    $vquery = $this ->
                    conexion-> prepare( $sql );
                    $vquery-> bindParam( 1, $email, PDO::PARAM_STR );
                    $vquery-> execute();

                    while ( $row = $vquery -> fetch(PDO::FETCH_ASSOC ) )
                    {
                        return $row;
                    }

                }
            }
            catch ( PDOException $e )
            {
                return  array( "error"=>true, "resultado"=>$e->getMessage()  );
            }
        }

        public function resetaer_usuario( $usuario )
        {
            try
            {
                $sql = " UPDATE   usuarios
                            SET   password = MD5( 'temporal' ),                                  
                                  fecha_ultimo_cambio = CURRENT_TIMESTAMP,
                                  modificado_por = :usuario 
                          WHERE   usuario = :usuario";

                $vquery = $this->
                conexion->prepare( $sql );
                $vquery->bindParam( ":usuario", $usuario, PDO::PARAM_STR );
                $vquery->execute();
            }
            catch ( PDOException $e )
            {
                return  array( "error"=>true, "resultado"=>$e->getMessage()  );
            }

        }
    }