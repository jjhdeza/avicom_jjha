<?php

require_once( dirname(__FILE__,2).'/models/modelo_session.php' );

class sesion
{
    public static function validar_usuario( $usuario,$password )
    {
        try
        {
            $modelo = new modelo_session();
            $datauser = $modelo -> obtener_acceso($usuario, $password, '', 1);

            if (!isset($datauser["error"]))
            {
                if ((int)$datauser["total"] > 0)
                {
                    if ((int)$datauser["vigencia"] > 0)
                    {
                        $response["error"] = false;
                        $response["resultado"] = "Usaurio valido";
                    }
                    else
                    {
                        $response["error"] = true;
                        $response["resultado"] = "Usuario no vigente";
                    }
                }
                else
                {
                    $response["error"] = true;
                    $response["resultado"] = "Usuario o contraseña invalidos";
                }
            }
            else
            {
                $response = $datauser;
            }

            return $response;


        }
        catch ( Exception $e )
        {
            return array( "error"=>true, "resultado"=>$e->getMessage() );
        }
    }

    public static function recuperar_usuario( $data )
    {
        try
        {
            $modelo = new modelo_session();

            foreach ( $data as $value )
            {
                $correo  = ( isset( $value->email ) ? trim( $value->email ):"" );
            }

            $datauser = $modelo->obtener_acceso( '', '' , $correo, 2);

            if( empty( $datauser ) )
                throw new Exception( "usuario o cuenta de correo electrónico no registrado" );

            $modelo->resetaer_usuario( $datauser["usuario"] );
            return self::emitir_correo( $datauser );

        }
        catch ( Exception $e )
        {
            return array( "error"=>true, "validation"=>"validation", "resultado"=>$e->getMessage() );
        }
    }

    public static function emitir_correo( $datos_usuario )
    {
        try
        {
            require ( dirname(__FILE__,5).'/libs/PHPMailer/src/PHPMailer.php');
            require ( dirname(__FILE__,5).'/libs/PHPMailer/src/Exception.php');
            require ( dirname(__FILE__,5).'/libs/PHPMailer/src/SMTP.php');

            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->IsSMTP(); // enable SMTP

            /*
               * Introduce la verificación del certificado SSL... en este caso, en nuestro servidor de correo estamos utilizando un servidor no certificado por ninguna empresa certificado,
               * La solución ha sido, desactivar la verificación mediante las opciones del SMTP
              */

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587

            $mail->Username = "xxxxxxxxxxx@gmail.com"; //Correo Emisor
            $mail->Password = "xxxxxxxxxxx";//Contraseña del Correo Emisor
            $mail->SetFrom("xxxxxxxxxxxxxx@gmail.com");

            $mail->IsHTML(true);
            $mail->Subject = utf8_decode( 'Recuperación de Usuario' );
            $mail->Body    = 'Gracias, hemos recibo tu solictud, tus datos son: usuario: '.$datos_usuario["usuario"].'<br> password: temporal.<br>Saludos coordiales...!';
            $mail->AddAddress( $datos_usuario["email"] );

            if ( $mail->send() )
                return array( "error"=>false, "validation"=>"validation", "resultado"=>"Correo enviado" );
            else
                throw new Exception( $mail->ErrorInfo );
        }
        catch ( Exception $e )
        {
            return array( "error"=>true, "validation"=>"exception-email", "response"=>$e->getMessage() );
        }
    }
}