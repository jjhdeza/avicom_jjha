<?php

    require_once( dirname(__FILE__,5).'/db/Conexion.php' );

    class models_admin_user
    {
        protected $conexion;

        public function __construct()
        {
            $conexion = new Conexion();
            $this->conexion = $conexion->conexion_avicom();
        }

        public function obtener_usuarios( )
        {
            try
            {
                $sql = "   SELECT   s.cve_usuario
                                    ,s.usuario
                                    ,UPPER( CONCAT( s.nombre, ' ', s.apellido_paterno,' ', s.apellido_materno ) ) AS fullname
                                    ,s.nombre
                                    ,s.apellido_paterno
                                    ,s.apellido_materno
                                    ,s.email
                                    ,s.fecha_vigencia_final AS fecha_vigencia 
                                    ,e.cve_estatus
                                    ,e.nombre_estatus                                
                             FROM   usuarios s
                        LEFT JOIN   cat_estatus e ON s.cve_estatus = e.cve_estatus 
                            WHERE   1 = 1 ";

                $vquery = $this ->
                conexion-> prepare( $sql );
                $vquery-> execute();
                $data = [];

                while ( $row = $vquery -> fetch(PDO::FETCH_ASSOC ) )
                {
                    $data[] =  $row;
                }

                return $data;


            }
            catch ( PDOException $e )
            {
                return  array( "error"=>true, "resultado"=>$e->getMessage()  );
            }
        }

        public function registrar_usuario($usuario,$password,$nombre,$appaterno,$apmaterno,$correo )
        {
            try
            {
                $sql = " INSERT INTO usuarios ( usuario, password, nombre, apellido_paterno, apellido_materno, email, fecha_vigencia_inicial, fecha_vigencia_final, cve_estatus )
                              VALUES ( ?, MD5( ? ), UPPER( TRIM( ?) ), UPPER( TRIM( ? ) ), UPPER( TRIM( ? ) ), LOWER ( TRIM( ? ) ), CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1 )";
                $vquery = $this->
                conexion->prepare( $sql );
                $vquery->bindParam( 1, $usuario, PDO::PARAM_STR );
                $vquery->bindParam( 2, $password, PDO::PARAM_STR );
                $vquery->bindParam( 3, $nombre, PDO::PARAM_STR );
                $vquery->bindParam( 4, $appaterno, PDO::PARAM_STR );
                $vquery->bindParam( 5, $apmaterno, PDO::PARAM_STR );
                $vquery->bindParam( 6, $correo, PDO::PARAM_STR );
                $vquery->execute();

                return array( "error"=>false, "validation"=>"validation", "resultado"=>"Usuario registrado correctamente" );
            }
            catch ( PDOException $e )
            {
                return  array( "error"=>true, "resultado"=>$e->getMessage()  );
            }
        }

        public function actualizar_usuario( $cve_usuario, $password,$nombre,$appaterno,$apmaterno,$correo, $usuario_reg )
        {
            try
            {
                $sql = " UPDATE   usuarios
                            SET   password = MD5( ? ), 
                                  nombre = UPPER( TRIM( ?) ),
                                  apellido_paterno = UPPER( TRIM( ?) ),
                                  apellido_materno = UPPER( TRIM( ?) ),
                                  email = LOWER( TRIM( ? ) ),             
                                  fecha_ultimo_cambio = CURRENT_TIMESTAMP,
                                  modificado_por = ? 
                          WHERE   cve_usuario = ? ";

                $vquery = $this->
                conexion->prepare( $sql );
                $vquery->bindParam( 1, $password, PDO::PARAM_STR );
                $vquery->bindParam( 2, $nombre, PDO::PARAM_STR );
                $vquery->bindParam( 3, $appaterno, PDO::PARAM_STR );
                $vquery->bindParam( 4, $apmaterno, PDO::PARAM_STR );
                $vquery->bindParam( 5, $correo, PDO::PARAM_STR );
                $vquery->bindParam( 6, $usuario_reg, PDO::PARAM_STR );
                $vquery->bindParam( 7, $cve_usuario, PDO::PARAM_INT );
                $vquery->execute();

                return array( "error"=>false, "validation"=>"validation", "resultado"=>"Datos actualizados" );
            }
            catch ( PDOException $e )
            {
                return  array( "error"=>true, "resultado"=>$e->getMessage()  );
            }
        }

        public function inhabilitar_usuario( $cve_usuario,$cve_estatus, $usuario_reg )
        {
            try
            {
                $sql = " UPDATE   usuarios
                            SET   cve_estatus = ?,                                  
                                  fecha_ultimo_cambio = CURRENT_TIMESTAMP,
                                  modificado_por = ? 
                          WHERE   cve_usuario = ?";

                $vquery = $this->
                conexion->prepare( $sql );
                $vquery->bindParam( 1, $cve_estatus, PDO::PARAM_INT );
                $vquery->bindParam( 2, $usuario_reg, PDO::PARAM_STR );
                $vquery->bindParam( 3, $cve_usuario, PDO::PARAM_INT );
                $vquery->execute();

                return array( "error"=>false, "validation"=>"validation", "resultado"=>"Usuario actualizado" );
            }
            catch ( PDOException $e )
            {
                return  array( "error"=>true, "resultado"=>$e->getMessage()  );
            }
        }
    }