<?php

require_once( dirname(__FILE__,2).'/models/models_admin_user.php' );

class classAdmin_user
{
    public static function getUsuarios()
    {
        try
        {
            $modelo   = new models_admin_user();
            $usuarios = $modelo->obtener_usuarios();

            return  array( "error"=>false, "usuarios"=>$usuarios ) ;
        }
        catch ( PDOException $e )
        {
            return  array( "error"=>true, "resultado"=>$e->getMessage()  );
        }
    }

    public static function registrar_usuario( $data )
    {
        try
        {
            $modelo = new models_admin_user();

            foreach ( $data as $value )
            {
                $usuario   = ( isset( $value->usuario ) ? trim( $value->usuario ):"" );
                $password  = ( isset( $value->password ) ? trim( $value->password ):"" );
                $nombre    = ( isset( $value->nombre ) ? trim( $value->nombre ):"" );
                $appaterno = ( isset( $value->apellido_paterno ) ? trim( $value->apellido_paterno ):"" );
                $apmaterno = ( isset( $value->apellido_materno ) ? trim( $value->apellido_materno ):"" );
                $correo    = ( isset( $value->email ) ? trim( $value->email ):"" );
            }

            return $modelo->registrar_usuario( $usuario,$password,$nombre,$appaterno,$apmaterno,$correo );


        }
        catch ( PDOException $e )
        {
            return  array( "error"=>true, "resultado"=>$e->getMessage()  );
        }
    }

    public static function actualizar_usuario( $data, $usuario_reg )
    {
        try
        {
            $modelo = new models_admin_user();

            foreach ( $data as $value )
            {
                $cve_usuario = ( isset( $value->cve_usuario ) ? trim( $value->cve_usuario ):"" );
                $password    = ( isset( $value->password ) ? trim( $value->password ):"" );
                $nombre      = ( isset( $value->nombre ) ? trim( $value->nombre ):"" );
                $appaterno   = ( isset( $value->apellido_paterno ) ? trim( $value->apellido_paterno ):"" );
                $apmaterno   = ( isset( $value->apellido_materno ) ? trim( $value->apellido_materno ):"" );
                $correo      = ( isset( $value->email ) ? trim( $value->email ):"" );
            }

            return $modelo->actualizar_usuario( $cve_usuario,$password,$nombre,$appaterno,$apmaterno,$correo,$usuario_reg );


        }
        catch ( PDOException $e )
        {
            return  array( "error"=>true, "resultado"=>$e->getMessage()  );
        }
    }

    public static function vigencia_usuario( $data, $usuario_reg )
    {
        try
        {
            $modelo = new models_admin_user();

            foreach ( $data as $value )
            {
                $cve_usuario = ( isset( $value->cve_usuario ) ? trim( $value->cve_usuario ):"" );
                $tipo = ( isset( $value->tipo ) ? trim( $value->tipo ):"" );
            }

            $cve_estatus = ( $tipo === 'activar' ) ? 1 : 2;

            return $modelo->inhabilitar_usuario( $cve_usuario, $cve_estatus, $usuario_reg );

        }
        catch ( PDOException $e )
        {
            return  array( "error"=>true, "resultado"=>$e->getMessage()  );
        }
    }
}