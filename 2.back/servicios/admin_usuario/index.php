<?php

    include ( '../class/class_headers.php' );
    include ( 'Servicios/class/class_admin_user.php' );

    header('Content-type: application/json; charset=utf-8');
    class_headers::obtener_headers();
    $header = class_headers::validar_authorizacion();

    if( !$header["error"] )
    {
        $response = [];
        $headers = getallheaders();

        $servicio = $headers["servicio"];

        switch ( $headers["servicio"] )
        {
            case "consulta":
                $response = classAdmin_user::getUsuarios();
                break;

            case "registrar":
                $data     = json_decode( file_get_contents('php://input') );
                $response = classAdmin_user::registrar_usuario( $data );
                break;

            case "actualizar":
                $usuario  = base64_decode( $header["response"]->usuario );
                $data     = json_decode( file_get_contents('php://input') );
                $response = classAdmin_user::actualizar_usuario( $data, $usuario );
                break;

            case "vigencia":
                $usuario  = base64_decode( $header["response"]->usuario );
                $data     = json_decode( file_get_contents('php://input') );
                $response = classAdmin_user::vigencia_usuario( $data, $usuario );
                break;

        }

        header("HTTP/1.1 200 OK");
        echo json_encode( array( "admins"=>$response ) );
    }
    else
    {
        header("HTTP/1.1 200 OK");
        echo json_encode( array("accesos"=>array('error'=>false, "validaction"=>"validation", "accesos" =>$header["response"] ) ) );
    }