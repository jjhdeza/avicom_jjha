<?php

/**
 * @Clase para definición de Control de CORS
 * @autor: Juan José Hernández Antonio
 **/

class class_headers
{
    //Función para definición de Cabeceras de Peticción HTTP, CORS
    public static function obtener_headers ()
    {
        try
        {
            if ( isset( $_SERVER['HTTP_ORIGIN'] ) )
            {
                // Decide si el origen en $ _SERVER ['HTTP_ORIGIN'] es uno, si se requiere permitir, y si es así:
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // Caducidad ( Cache por un día)
            }

            // Los encabezados de control de acceso se reciben durante las solicitudes de OPCIONES
            if ( $_SERVER['REQUEST_METHOD'] === 'OPTIONS' )
            {
                if ( isset( $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] ) )
                    header("Access-Control-Allow-Methods: GET, POST");

                if ( isset( $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'] ) )
                    header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
        }
        catch ( PDOException $e )
        {
            return $e->getMessage();
        }
    }
    //Termina Función para definición de Cabeceras de Peticción HTTP, CORS

    //Termina Función para Obtener Validación de Authorization
    public static function validar_authorizacion()
    {
        try
        {
            $headers = getallheaders();

            if( !isset( $headers["Authorization"] ) || empty( $headers["Authorization"] ) )
                return array( "error" => true, "response"=>"Autorización no es correcta");

            if( !isset( $headers["x-session-code"] ) || empty( $headers["x-session-code"] ) || (int)$headers["x-session-code"] !== 1516 )
                return array( "error" => true, "response"=>"Sin acceso a este servicio");

            if( !isset( $headers["Origin"] ) || empty( $headers["Origin"] ) || $headers["Origin"] !== "http://localhost:4200" )
                return array( "error" => true, "response"=>"Sin persimos de acceso");

            if( $_SERVER["REQUEST_METHOD"] === 'GET' || $_SERVER["REQUEST_METHOD"] === 'POST' )
            {
                $authorization = explode(' ', $headers["Authorization"] );
                return array( "error" => false, "response"=> json_decode( base64_decode( $authorization[1] ) ) );
            }
            else
            {
                return array( "error" => true, "response"=>"Accesos de petición no permitido");
            }
        }
        catch ( Exception $e )
        {
            return array( "error" => true, "response"=> $e->getMessage() );
        }
    }
}