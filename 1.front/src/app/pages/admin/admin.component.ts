import { Component, OnInit } from '@angular/core';
import { AdminUserService  } from '../../services/admin_user/admin.user.service';
import {ConfirmationService, MessageService, PrimeNGConfig} from 'primeng/api';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [ MessageService, ConfirmationService ]
})
export class AdminComponent implements OnInit {

  public usuarios   = [];
  public validacion =
  {
    lbl_modal : "",
    tipo      : "",
    modal     : false,
    error_reg : false
  };

  public usuario =
  {
    cve_usuario : null,
    usuario : null,
    password : null,
    password_confirm : null,
    nombre : null,
    apellido_paterno : null,
    apellido_materno : null,
    email : null
  };

  ngOnInit(): void
  {}

  constructor
  (
    private _adminService  : AdminUserService,
    private messageService : MessageService,
    private confirmService : ConfirmationService,
    private primengConfig  : PrimeNGConfig,
    private _routerLink    : Router
  )
  {
    this.primengConfig.ripple = true;
    this.consultar();
  }

  public consultar()
  {
    this._adminService.obtenerUsuarios()
      .subscribe( ( params : any )=>
      {
        if( !params.error)
        {
          this.usuarios = params.usuarios;
        }
      });
  }

  nuevo_usuario()
  {
    this.validacion.lbl_modal = "Nuevo usuario";
    this.validacion.tipo      = "NUEVO";
    this.validacion.modal     = !this.validacion.modal;

    this.usuario =
      {
        cve_usuario : null,
        usuario : null,
        password : null,
        password_confirm : null,
        nombre : null,
        apellido_paterno : null,
        apellido_materno : null,
        email : null
      };
  }

  validar_usuario()
  {
    this.validacion.error_reg = false;
    if( this.validacion.tipo === 'NUEVO' )
    {
      if( this.usuario.usuario !== null )
      {
        this.usuario.usuario = this.usuario.usuario.trim();

        Object.values( this.usuarios ).forEach( (value : any) =>
        {
          if( value.usuario === this.usuario.usuario )
            this.validacion.error_reg = true;
        });

      }
    }
  }

  registrar( frmUser:NgForm )
  {
    if( frmUser.form.status === "INVALID" )
    {
      Object.values( frmUser.form.controls ).forEach( control => control.markAllAsTouched() );
      return;
    }

    this._adminService.registrar_usuario( this.usuario )
      .subscribe( ( response : any )=>
      {
        if( !response.error )
        {
          this.messageService.add({key: 'tc', severity:'success', summary: 'Success', detail: response.resultado });
          this.consultar();//Se acualiza lista de usuarios
          this.nuevo_usuario();
        }
      });
  }

  editar( index )
  {
    this.validacion.lbl_modal = "Actualiza datos de usuario";
    this.validacion.tipo      = "EDICION";
    this.validacion.modal = !this.validacion.modal;

    this.usuario =
      {
        cve_usuario : index.cve_usuario,
        usuario : index.usuario,
        password : null,
        password_confirm : null,
        nombre : index.nombre,
        apellido_paterno : index.apellido_paterno,
        apellido_materno : index.apellido_materno,
        email : index.email
      };

    console.log(  this.usuario );
  }

  inhabiliar( index )
  {

    this.confirmService.confirm({
      message: `<p>Deseas inactivar/activar el acceso al usuario: <b>${ index.usuario } </b>?</p>`,
      header: 'Confirmación',
      acceptLabel:"Continuar",
      rejectLabel:"Cancelar",
      icon: 'pi pi-exclamation-triangle',

      accept: () =>
      {
        const data =
          {
            cve_usuario : index.cve_usuario,
            tipo : ( index.cve_estatus === 2 )?'activar':'desactivar'
          }
        this._adminService.inactivar_usuario( data )
          .subscribe( ( response : any )=>
          {
            if( !response.error )
            {
              this.messageService.add({key: 'tc', severity:'success', summary: 'Success', detail: response.resultado });
              this.consultar();//Se acualiza lista de usuarios
            }
          });
      },
      reject: () => { }
    });
  }

  terminar_sesion( )
  {
    this.confirmService.confirm({
      message: `<p>Deseas terminar la sesi&oacute;n actual?</p>`,
      header: 'Confirmación',
      acceptLabel:"Continuar",
      rejectLabel:"Cancelar",
      icon: 'pi pi-exclamation-triangle',
      accept: () =>
      {
        localStorage.removeItem('token' );
        this._routerLink.navigateByUrl('login').then();
      },
      reject: () => { }
    });
  }


}



