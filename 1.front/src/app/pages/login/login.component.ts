import { Component, OnInit, Renderer2 } from '@angular/core';
import { AutenticacionService } from '../../services/autenticacion/autenticacion.service';
import {ConfirmationService, Message, MessageService} from 'primeng/api';
import { Router } from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[ MessageService, ConfirmationService ],
})
export class LoginComponent implements OnInit {

  public mdm_usuario :boolean = false;
  public msgs2: Message[];
  public msgs: Message[];

  public usuario:any =
    {
      usuario  : null,
      password : null,
      correo   : null
    };

  constructor
  (
    private _authService   : AutenticacionService,
    private confirmService : ConfirmationService,
    private messageService : MessageService,
    private _routerLink    : Router
  )
  {}

  ngOnInit(): void {
  }

  public login()
  {
    this.msgs2 = [];
    const data = btoa( JSON.stringify( { usuario : btoa( this.usuario.usuario ), password: btoa( this.usuario.password ) } ) );

    this._authService.iniciar_sesion( data )
      .subscribe( ( response : any)=>
      {
        if( !response.error )
        {
          this._authService.guardarToken( data );
          this._routerLink.navigateByUrl('control/admin').then();
        }
        else
        {
          this.msgs2 = [ {severity:'error', summary:'Error', detail: response.resultado } ];
        }
      });
  }

  modal_usuario()
  {
    this.mdm_usuario = !this.mdm_usuario;
    this.usuario =
      {
        usuario  : null,
        password : null,
        correo   : null
      };
  }

  public recuperar_usuario( frmRep : NgForm )
  {
    this.msgs = [];

    if( frmRep.form.status === "INVALID" )
    {
      Object.values( frmRep.form.controls ).forEach( control => control.markAllAsTouched() );
      return;
    }

    this._authService.recuperar_usuario( { email : this.usuario.email })
      .subscribe( ( response : any )=>
      {
        if( !response.error )
        {
          this.messageService.add({key: 'tc', severity:'success', summary: 'Success', detail: response.resultado });
        }
        else
        {
          if( response.validation === 'validation' )
            this.msgs = [ {severity:'error', summary:'Error', detail: response.resultado } ];
          else
            console.error( response );
        }

      });
  }
}
