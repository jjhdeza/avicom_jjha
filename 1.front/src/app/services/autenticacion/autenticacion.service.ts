import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { delay, map } from "rxjs/operators";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService
{
  private  apiUrl: string = environment.URL_API;

  constructor
  (
    private _httpClient : HttpClient,
  ) { }

  public iniciar_sesion ( data:string )
  {
      const headers = new HttpHeaders ({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Methods': 'GET, POST',
        'Authorization': 'Bearer ' + data,
        'x-session-code': environment.CODE_SESSION_PASSPORT.toString()
      });

      return this._httpClient.get( `${ this.apiUrl }/login/`, { headers } )
        .pipe(
          map( ( response : any ) =>
          {
            return response.session;
          })
        );
  }

  public guardarToken ( token  )
  {
    let init_session = new Date();
    init_session.setSeconds( 3600 );
    localStorage.setItem( 'token', token  );

    //Tiempo disponible para la sessión
    localStorage.setItem( 'instance', init_session.getTime().toString()  );

    return true;
  }

  public autenticacionActiva () : boolean
  {
    const token    = ( localStorage.getItem( 'token' ) === "" || localStorage.getItem( 'token' ) == null ) ? "" : localStorage.getItem( 'token' );
    const vigencia = Number( localStorage.getItem('instance') );
    const expira   = new Date();

    expira.setTime( vigencia );

    if( token !== "" )
      return expira > new Date();
    else
      return false;
  }

  public recuperar_usuario( parametros )
  {
    const headers = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'GET, POST',
      'Authorization': 'Bearer ',
      'x-session-code': environment.CODE_SESSION_PASSPORT.toString(),
      'servicio': 'recuperacion'
    });

    return this._httpClient.post( `${ this.apiUrl }/login/`, { data: parametros }, { headers } )
      .pipe(
        map( ( response : any ) =>
        {
          return response.session;
        })
      );
  }



}
