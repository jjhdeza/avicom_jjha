import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { delay, map } from "rxjs/operators";
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AdminUserService
{

  private  apiUrl: string = environment.URL_API;

  constructor
  (
    private _httpClient : HttpClient
  )
  {}

  public obtenerUsuarios()
  {
    const headers = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'GET, POST',
      'Authorization': 'Bearer ' + localStorage.getItem('token'),
      'x-session-code': environment.CODE_SESSION_PASSPORT.toString(),
      'servicio': 'consulta'
    });

    return this._httpClient.get( `${ this.apiUrl }/admin_usuario/`, { headers } )
      .pipe(
        map( ( response : any ) =>
        {
          return response.admins;
        })
      );
  }

  public registrar_usuario( parametros )
  {
    const headers = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'GET, POST',
      'Authorization': 'Bearer ' + localStorage.getItem('token'),
      'x-session-code': environment.CODE_SESSION_PASSPORT.toString(),
      'servicio': ( parametros.cve_usuario === null )?'registrar':'actualizar'
    });

    return this._httpClient.post( `${ this.apiUrl }/admin_usuario/`, { data: parametros }, { headers } )
      .pipe(
        map( ( response : any ) =>
        {
          return response.admins;
        })
      );
  }

  public inactivar_usuario( parametros )
  {
    const headers = new HttpHeaders ({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'GET, POST',
      'Authorization': 'Bearer ' + localStorage.getItem('token'),
      'x-session-code': environment.CODE_SESSION_PASSPORT.toString(),
      'servicio': 'vigencia'
    });

    return this._httpClient.post( `${ this.apiUrl }/admin_usuario/`, { data: parametros }, { headers } )
      .pipe(
        map( ( response : any ) =>
        {
          return response.admins;
        })
      );
  }
}
