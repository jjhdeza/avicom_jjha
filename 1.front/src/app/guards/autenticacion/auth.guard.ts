import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AutenticacionService} from '../../services/autenticacion/autenticacion.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor( private _authService : AutenticacionService, private _router: Router ) {
  }

  //Autenticación para activar a las rutas a la que se desea navegar
  canActivate( ) : boolean
  {
    if( !this._authService.autenticacionActiva() )
    {
      this._router.navigateByUrl('/login').then();
      return false;
    }
    else
    {
      return true;
    }
  }

}
