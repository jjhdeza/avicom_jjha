import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AuthGuard } from './guards/autenticacion/auth.guard';

const routes: Routes =
  [
    { path: 'login', component: LoginComponent, },
    { path: 'control', canActivate: [ AuthGuard ] , children : [ { path: 'admin', component: AdminComponent, } ] },
    { path: '**', pathMatch: 'full' , redirectTo: 'login' }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
