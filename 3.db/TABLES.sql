 
 CREATE TABLE `test_avicom`.`cat_estatus`
 (
 	`cve_estatus` INT(20) NOT NULL,
 	`codigo_estatus` VARCHAR(20) NOT NULL,
 	`nombre_estatus` VARCHAR(50) NOT NULL,
    `descripcion_estatus`  VARCHAR(100) NOT NULL
 );
 
ALTER TABLE cat_estatus ADD PRIMARY KEY ( cve_estatus );
 
CREATE TABLE `test_avicom`.`usuarios`
( 
	`cve_usuario` INT(20) NOT NULL,
	`usuario` VARCHAR(50) NOT NULL,
	`password` VARCHAR(50) NOT NULL,
	`nombre` VARCHAR(100) NOT NULL , 
	`apellido_paterno` VARCHAR(100) NOT NULL ,
	`apellido_materno` VARCHAR(100) NOT NULL ,
	`email` VARCHAR(100) NOT NULL ,
	`fecha_vigencia_inicial` DATE NOT NULL,
	`fecha_vigencia_final` DATE NOT NULL, 
	`cve_estatus` INT(20) NOT NULL , 
	`fecha_registro` DATE NOT NULL, 
	`creado_por` VARCHAR(50), 
	`fecha_ultimo_cambio` DATE NOT NULL, 
	`modificado_por` VARCHAR(50)	
);
--CURRENT_TIMESTAMP
 ALTER TABLE usuarios ADD PRIMARY KEY ( cve_usuario );
 ALTER TABLE usuarios MODIFY cve_usuario INT unsigned auto_increment;
 ALTER TABLE usuarios FOREIGN KEY (cve_estatus) REFERENCES cat_estatus(cve_estatus);

 ALTER TABLE usuarios AUTO_INCREMENT=1;

 INSERT INTO cat_estatus ( cve_estatus, codigo_estatus, nombre_estatus, descripcion_estatus) VALUES( 1,'VIG', 'VIGENTE','USUARIO VIGENTE');
 INSERT INTO cat_estatus ( cve_estatus, codigo_estatus, nombre_estatus, descripcion_estatus) VALUES( 2,'NVI', 'NO VIGENTE','USUARIO NO VIGENTE');
 INSERT INTO cat_estatus ( cve_estatus, codigo_estatus, nombre_estatus, descripcion_estatus) VALUES( 3,'SRL', 'SIN ROLES DES ACCESSO','SIN ROLES DES ACCESSO');

 INSERT INTO usuarios ( usuario, password, nombre, apellido_paterno, apellido_materno, email, fecha_vigencia_inicial, fecha_vigencia_final, cve_estatus )
 VALUES ( 'juanjose', MD5('temporal'), UPPER( TRIM( 'JUAN JOSE' ) ), UPPER( TRIM( 'HDEZ' ) ), UPPER( TRIM( 'ANTONIO' ) ), LOWER( TRIM( 'jjhernandeza@gmail.com' ) ),  CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1 );
